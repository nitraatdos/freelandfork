<?php
/*
 * Name: FreeLand v2
 * Author: Marko Murumaa
 * Created: July 2015
 * Website: www.freelandplay.eu
*/

require_once("core/init.php");

include("includes/overall/header.php");
?>
<div id="page">
	<div class="page-title">Kontakt</div>
	<p>
		<?php
		if (empty($_GENERAL->errors()) === false) {
			print($_GENERAL->output_errors());
		}

		if (Session::exists('contact')) {
			$_GENERAL->addOutSuccess(Session::flash('contact'));
			print($_GENERAL->output_success());
		}
		?>
	<table>
		<tr valign="top">
			<td width="20%"><img src="css/default/images/contact.png" width="100" height="100"></td>
			<td width="80%">
				Siin on kirjas kõik vajalik, et meiega ühendust saada.

			</td>
		</tr>
	</table>
	<b>Rauno Moisto</b> - mängu omanik, programmeerija<br>
	E-mail: <b><?php print($_GENERAL->settings('settings_game', 'GAME_EMAIL')); ?></b><br>
	<br>
	Mängu reeglite rikkumise kaebused saata e-maili aadressile <b><?php print($_GENERAL->settings('settings_game', 'GAME_EMAIL')); ?></b><br>
	Mängu <b>FreeLand</b> reeglid <a href="p.php?p=rules">loe siit</a>.<br><br>
	<b>NB! Kindlasti märkida kirja sisus ära ka kasutajanimi, sest muidu me ei saa aidata!</b><br>

	</p>
</div>
<?php
include("includes/overall/footer.php");
